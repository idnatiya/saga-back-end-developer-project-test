<div class="modal fade" tabindex="-1" role="dialog" id="modalForm">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="{{ route('admin.user.store') }}" method="POST" onsubmit="doSubmit(this, event)">
				<input type="hidden" name="_method">
				<input type="hidden" name="id">
				@csrf
				<div class="modal-header">
					<h5 class="modal-title">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Full Name <span class="text-danger">*</span></label>
						<input type="text" name="name" class="form-control" autocomplete="off" tabindex="1"> 
					</div>
					<div class="form-group">
						<label>Email <span class="text-danger">*</span></label>
						<input type="text" name="email" class="form-control" autocomplete="off" tabindex="2"> 
					</div>
					<div class="form-group">
						<label>Role <span class="text-danger">*</span></label>
						<select name="role" class="form-control" tabindex="3">
							<option value="">Select Role</option>
							@foreach ($roles as $role)
								<option value="{{ $role['id'] }}">{{ $role['name'] }}</option>
							@endforeach 
						</select>
					</div>
					<div class="form-group">
						<label>Password <span class="text-danger">*</span></label>
						<input type="password" name="password" class="form-control" autocomplete="off" tabindex="4"> 
						<small class="d-none text-muted password-description">** Leave it blank if you don't want to change the password</small>
					</div>
					<div class="form-group">
						<label>Password Confirmation <span class="text-danger">*</span></label>
						<input type="password" name="password_confirmation" class="form-control" autocomplete="off" tabindex="5"> 
						<small class="d-none text-muted password-description">** Leave it blank if you don't want to change the password</small>
					</div>
				</div>
				<div class="modal-footer bg-whitesmoke br">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" tabindex="6" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>