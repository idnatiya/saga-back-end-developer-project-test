<?php 

namespace App\Http\Controllers\Admin; 

use App\Repositories\UserRepository; 
use App\Repositories\RoleRepository; 
use App\Http\Controllers\Admin\Controller as BaseController; 
use App\Http\Requests\Admin\User\TableRequest; 
use App\Http\Requests\Admin\User\StoreRequest; 
use App\Http\Requests\Admin\User\UpdateRequest; 
use App\Http\Requests\Admin\User\DeleteRequest; 
use App\Services\UserService; 

class UserController extends BaseController
{
	protected $userRepository; 
	protected $roleRepository;
	protected $userService; 

	/**
	 * Construct
	 */
	public function __construct(
		UserRepository $userRepository,
		RoleRepository $roleRepository,
		UserService $userService
	)
	{
		$this->userRepository = $userRepository; 
		$this->roleRepository = $roleRepository; 
		$this->userService = $userService; 
	}

	/**
	 * Index
	 * 
	 * @return void
	 */
	public function index()
	{
		return view('admin::user.index', [
			'roles' => $this->roleRepository->findAll(), 
		]); 
	}

	/**
	 * Tables 
	 * 
	 * @return Illuminate\Response\Json
	 */
	public function tables(TableRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		$dataTables = $this->userRepository->getCmsTable($requestData);

		return $dataTables;  
	}

	/**
	 * Store 
	 * 
	 * @param  StoreRequest $requestData 
	 * @return Illuminate\Response\Json                  
	 */
	public function store(StoreRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		$process = $this->userService->doStore($requestData); 

		return response()->json([
			'status' => $process['status'],
			'message' => $process['message'],
		], $process['responseCode']); 
	}

	/**
	 * Update 
	 * 
	 * @param  UpdateRequest $requestData 
	 * @return Illuminate\Response\Json                  
	 */
	public function update(UpdateRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		$process = $this->userService->doUpdate($requestData); 

		return response()->json([
			'status' => $process['status'],
			'message' => $process['message'],
		], $process['responseCode']); 
	}

	/**
	 * Delete 
	 * 
	 * @param  Delete $requestData 
	 * @return Illuminate\Response\Json                  
	 */
	public function delete(DeleteRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		dd($requestData);
		$process = $this->userService->doDelete($requestData); 

		return response()->json([
			'status' => $process['status'],
			'message' => $process['message'],
		], $process['responseCode']); 
	}
}