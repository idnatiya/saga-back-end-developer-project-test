<?php 

namespace App\Http\Controllers; 

use App\Repositories\PostRepository; 

class HomeController extends Controller
{
    protected $postRepository; 

    /**
     * Construct 
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository; 
    }

    /**
     * Index
     */
    public function index()
    { 
        return view('web::home', [
            'lastestPost' => $this->postRepository->getLastPosts(), 
        ]); 
    }
}