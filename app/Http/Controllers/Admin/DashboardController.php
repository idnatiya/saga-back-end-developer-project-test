<?php 

namespace App\Http\Controllers\Admin; 

use App\Http\Controllers\Admin\Controller as BaseController;

class DashboardController extends BaseController
{
	/**
	 * Index
	 * 
	 * @return void
	 */
	public function index()
	{
		return view('admin::dashboard.index'); 
	}
}