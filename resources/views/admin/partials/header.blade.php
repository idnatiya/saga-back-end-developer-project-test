<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
	<form class="form-inline mr-auto">
		<ul class="navbar-nav mr-3">
			<li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
		</ul>
	</form>
	<ul class="navbar-nav navbar-right">
		<li class="dropdown">
			<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
				@if (Auth::user()->avatar)
					<img alt="image" src="{{ Storage::disk('public')->url(Auth::user()->avatar) }}" class="rounded-circle mr-1">
				@else
					<img alt="image" src="{{ asset('assets-admin') }}/img/avatar/avatar-1.png" class="rounded-circle mr-1">
				@endif
				<div class="d-sm-none d-lg-inline-block">Hi, {{ Auth::user()->name }}</div>
			</a>
			<div class="dropdown-menu dropdown-menu-right">
				<a href="{{ route('admin.index') }}" class="dropdown-item has-icon">
					<i class="far fa-user"></i> My Profile
				</a>
				<div class="dropdown-divider"></div>
				<a href="{{ route('admin.auth.logout') }}" class="dropdown-item has-icon text-danger">
					<i class="fas fa-sign-out-alt"></i> Logout
				</a>
			</div>
		</li>
	</ul>
</nav>