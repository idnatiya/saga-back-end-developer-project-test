@extends('admin::layouts.mainapp')

@section('title', 'List All Post')

@section('content')
	<div class="section">
		<div class="section-header d-flex justify-content-between">
			<h1>Posts</h1>
			<div class="create-button">
				<a href="{{ route('admin.post.create') }}" class="btn btn-success">
					<i class="fa fa-fw fa-plus"></i> Create New Post
				</a>
			</div>	
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header">
							<form class="form-inline" action="" onsubmit="doSearch(event)">
								<div class="form-group">
									<input name="keyword" autocomplete="off" type="text" class="form-control" placeholder="Search Post ... ">
								</div>
								<button class="btn btn-info">
									<i class="fa fa-fw fa-search"></i> Search
								</button>
							</form>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-sm table-striped" id="table">
									<thead>
										<tr>
											<th>#</th>
											<th>Banner</th>
											<th>Title</th>
											<th>Category</th>
											<th>Author</th>
											<th>Status</th>
											<th class="text-center">Actions</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
									<tfoot>
										<tr>
											<th><input type="checkbox" name="checkAll"></th>
											<th colspan="6">
												<button type="button" class="btn btn-danger btn-sm" id="btn-deleted">
													<i class="fa fa-fw fa-trash"></i> Delete Selected Row
												</button>
											</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
						<div class="card-footer">
							<div id="pagination-container"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript">

		const modalForm = $('#modalForm');

		const doSearch = (event) => {
			event.preventDefault(); 
			table.loadDataTables(); 
		}; 

		/**
		 * Check All
		 */
		$('[name="checkAll"]').on('change', function () {
			elements = $('[name="id[]"]'); 
			$.each(elements, (_, element) => {
				$(element).prop('checked', this.checked); 
			}); 
		}); 

		/**
		 * Delete Checked
		 */
		$('#btn-deleted').on('click', function () {
			elements = $('[name="id[]"]:checked'); 
			if (elements.length == 0) {
				Swal.fire('Warning', 'No row selected', 'warning');
				return false;  
			}

			deletedRow = []; 
			$.each(elements, (_, element) => {
				deletedRow.push(element.value); 
			})

			showDeleteConfirmation()
				.then(result => {
					if (result.status) {
						doDelete(deletedRow); 
					} 
				}); 
		}); 

		/**
		 * Do Delete
		 */
		const doDelete = (deletedRow) => {
			window.showLoadingSwal(); 
			axios({
				url: `${window.baseURL}/admin/post`,
				method: 'DELETE', 
				data: {
					_token: `${window.csrfToken}`,
					id: deletedRow
				}
			})
			.then(resultJson => {
				Swal.close(); 
				Swal.fire('Success', resultJson.message, 'success'); 
				table.loadDataTables(); 
			})
			.catch(errorResponse => {
				Swal.close(); 
				handleErrorRequest(errorResponse); 
			}); 
		}; 

		/**
		 * Delete Row
		 */
		const deleteRow = (element, event) => {
			event.preventDefault(); 
			
			$el = $(element); 

			deletedRow = [$el.data('id')]; 

			showDeleteConfirmation()
				.then(result => {
					if (result.status) {
						doDelete(deletedRow); 
					} 
				}); 
		}	

		/**
		 * Table 
		 */
		const table = {
			baseURL: `{{ route('admin.post.tables') }}`, 
			element: $('#table'),
			setLoadingState() {
				this.element.find('tbody').html(`
					<tr class="text-warning">
						<td colspan="${this.element.find('thead th').length}">
							<i class="fa fa-fw fa-circle-notch fa-spin"></i> Please wait, loading data ...
						</td>
					</tr>
				`); 
			},
			render(resultJson) {
				let htmlRows = ``; 
				resultJson.forEach(row => {
					let htmlRow = $(`
						<tr>
							<td><input type="checkbox" value="${row.id}" name="id[]" /></td>
							<td class="td-banner"></td>							
							<td class="td-title"></td>							
							<td class="td-category"></td>							
							<td class="td-author"></td>							
							<td class="td-status"></td>							
							<td class="text-center">
								<a href="${window.baseURL}/admin/post/${row.id}/edit" class="btn btn-sm btn-warning">
									<i class="fa fa-fw fa-edit"></i>
								</a>
								<a href="#" data-id="${row.id}" onclick="deleteRow(this, event)" class="btn btn-sm btn-danger">
									<i class="fa fa-fw fa-trash"></i>
								</a>
							</td>
						</tr>
					`); 

					htmlRow.find('.td-banner').append(`<img src="${row.banner_thumb_url}" class="thumbnail" />`); 
					htmlRow.find('.td-title').text(row.title); 
					htmlRow.find('.td-category').text(row.category.name); 
					htmlRow.find('.td-author').text(row.author.name); 
					htmlRow.find('.td-status').append(
						row.status == 1 
						? 
							'<small class="badge badge-success">Publish</small>' 
						: 
							'<small class="badge badge-primary">Draft</small>'
					); 

					htmlRows += '<tr>' + htmlRow.html() + '</tr>';  
				}); 

				this.element.find('tbody').html(htmlRows); 
			}, 
			loadDataTables() {
				this.setLoadingState(); 
				axios({
					url: this.baseURL, 
					method: 'GET', 
					params: {
						keyword: $('input[name="keyword"]').val(), 
					}
				})
				.then(resultJson => {
					this.render(resultJson.data.data); 
					renderPagination(resultJson.data.links, $('#pagination-container'), this);
				})
				.catch(errorResponse => {
					handleErrorRequest(errorResponse); 
				}); 
			}
		};

		table.loadDataTables(); 

	</script>
@endpush