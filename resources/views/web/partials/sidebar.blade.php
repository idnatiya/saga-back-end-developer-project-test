<div class="sidebar-title for-tablet">Sidebar</div>
@php 
$instagramFeeds = \Facades\App\Helpers\InstagramHelper::get(); 
@endphp
<aside>
    <div class="aside-body">
        <div class="featured-author">
            @if ($instagramFeeds['status'])
                <div class="featured-author-inner">
                    <div class="featured-author-cover" style="background-image: url('{{ asset('assets-web') }}/images/news/img15.jpg');">
                        <div class="badges">
                            <div class="badge-item"><i class="ion-star"></i> Instagram Feed</div>
                        </div>
                        <div class="featured-author-center">
                            <figure class="featured-author-picture">
                                <img src="{{ $instagramFeeds['data']['picture'] }}" alt="{{ $instagramFeeds['data']['name'] }}">
                            </figure>
                            <div class="featured-author-info">
                                <h2 class="name">{{ $instagramFeeds['data']['name'] }}</h2>
                                <div class="desc">{{ '@'.$instagramFeeds['data']['username'] }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="featured-author-body">
                        <div class="block">
                            <h2 class="block-title">Photos</h2>
                            <div class="block-body">
                                <ul class="item-list-round" data-magnific="gallery">
                                    @foreach ($instagramFeeds['data']['feeds'] as $feed)
                                        <li><a href="{{ $feed['display'] }}" style="background-image: url('{{ $feed['thumbnail'] }}');"></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</aside>