<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\Eloquent\UserRepositoryEloquent::class); 
        $this->app->bind(\App\Repositories\RoleRepository::class, \App\Repositories\Eloquent\RoleRepositoryEloquent::class); 
        $this->app->bind(\App\Repositories\CategoryRepository::class, \App\Repositories\Eloquent\CategoryRepositoryEloquent::class); 
        $this->app->bind(\App\Repositories\PostRepository::class, \App\Repositories\Eloquent\PostRepositoryEloquent::class); 
    }
}
