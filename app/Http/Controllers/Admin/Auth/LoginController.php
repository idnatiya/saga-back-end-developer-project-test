<?php 

namespace App\Http\Controllers\Admin\Auth; 

use App\Http\Controllers\Admin\Controller as BaseController; 
use App\Http\Requests\Admin\Auth\SocialRequest; 
use Illuminate\Foundation\Auth\AuthenticatesUsers; 
use Illuminate\Support\Facades\Session; 
use Laravel\Socialite\Facades\Socialite;

class LoginController extends BaseController
{
	use AuthenticatesUsers; 

	/**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    /**
     * Socials 
     * 
     * @return void
     */
    public function socialRedirect(SocialRequest $request)
    {
        return Socialite::driver($request->route('platform'))->redirect(); 
    }

    /**
     * Social Callback
     * 
     * @param  SocialRequest $request 
     * @return void
     */
    public function socialCallback(SocialRequest $request)
    {
        try { 
            $platform = $request->route('platform'); 
            $callbackService = config('auth.socials.components.'.$platform.'.services'); 
            $callbackService = new $callbackService; 
            $process = $callbackService->handle(); 

            return redirect()->route('admin.index'); 
        } catch (Exception $e) {
            Session::flash('error_message', $e->getMessage()); 
            return redirect()->route('admin.auth.login.index'); 
        }
    }
}

