<footer class="main-footer">
	<div class="footer-left">
		Copyright &copy; 2022 <div class="bullet"></div> Theme By <a href="https://demo.getstisla.com/">Stisla Admin Panel</a>
	</div>
	<div class="footer-right">
		1.0
	</div>
</footer>