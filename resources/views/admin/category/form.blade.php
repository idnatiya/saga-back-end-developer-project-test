<div class="modal fade" tabindex="-1" role="dialog" id="modalForm">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="{{ route('admin.category.store') }}" method="POST" onsubmit="doSubmit(this, event)">
				<input type="hidden" name="_method">
				<input type="hidden" name="id">
				@csrf
				<div class="modal-header">
					<h5 class="modal-title"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Category Name <span class="text-danger">*</span></label>
						<input type="text" name="name" class="form-control" autocomplete="off" tabindex="1"> 
					</div>
				</div>
				<div class="modal-footer bg-whitesmoke br">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" tabindex="6" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>