<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
		<title>@yield('title')</title>
		<meta name="base-url" content="{{ url('/') }}">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- General CSS Files -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<!-- CSS Libraries -->
		<!-- Template CSS -->
		<link rel="stylesheet" href="{{ asset('assets-admin') }}/css/style.css">
		<link rel="stylesheet" href="{{ asset('assets-admin') }}/css/components.css">
		<link rel="stylesheet" href="{{ asset('assets-admin') }}/css/custom.css">
	</head>
	<body>
		<div id="app">
			<div class="main-wrapper main-wrapper-1">
				@include('admin::partials.header')
				<div class="main-sidebar">
					@include('admin::partials.sidebar')
				</div>
				<!-- Main Content -->
				<div class="main-content">
					@yield('content')
				</div>
				@include('admin::partials.footer')
			</div>
		</div>
		<!-- General JS Scripts -->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
		<script src="{{ asset('assets-admin') }}/js/stisla.js"></script>
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.26.1/axios.min.js" integrity="sha512-bPh3uwgU5qEMipS/VOmRqynnMXGGSRv+72H/N260MQeXZIK4PG48401Bsby9Nq5P5fz7hy5UGNmC/W1Z51h2GQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
		<script type="text/javascript">
			window.baseURL = $('meta[name="base-url"]').attr('content'); 
			window.csrfToken = $('meta[name="csrf-token"]').attr('content'); 
		</script>
		<!-- JS Libraies -->
		<!-- Template JS File -->
		<script src="{{ asset('assets-admin') }}/js/scripts.js"></script>
		<script src="{{ asset('assets-admin') }}/js/custom.js"></script>
		<!-- Page Specific JS File -->
		@stack('scripts')
	</body>
</html>