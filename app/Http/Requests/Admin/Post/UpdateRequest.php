<?php

namespace App\Http\Requests\Admin\Post;

use App\Repositories\PostRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth; 

class UpdateRequest extends FormRequest
{
    protected $postRepository; 

    /**
     * Construct 
     * 
     * @param PostRepository $postRepository 
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository; 
    }

    /**
     * Validate Access
     * 
     * @return void
     */
    protected function  validateAccess()
    {
        $post = $this->postRepository->find($this->id); 
        if ($post == null) {
            abort(404); 
        }

        if (Auth::user()->hasRole('Author')) {
            if ($post['user_id'] !== Auth::user()->id) {
                abort(403); 
            }
        }

        $this->merge(['post' => $post]); 
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->validateAccess(); 

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required|exists:posts,id', 
            'title'         => 'required|max:255', 
            'category_id'   => 'required|exists:categories,id', 
            'banner'        => 'mimes:jpg,png|max:2000', 
            'content'       => 'required', 
            'status'        => 'required',
            'post'          => '', 
        ];
    }
}
