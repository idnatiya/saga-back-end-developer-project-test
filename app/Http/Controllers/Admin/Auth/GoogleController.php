<?php 

namespace App\Http\Controllers\Admin\Auth; 

use App\Http\Controllers\Admin\Controller as BaseController;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Session;

class GoogleController extends BaseController
{
    /**
     * Redirect URI
     * 
     * @return void
     */
	public function redirect()
	{
		return Socialite::driver('google')->redirect();
	}

    /**
     * Handle Google Callback
     * 
     * @return void
     */
	public function handleGoogleCallback()
	{
		try {
            $user = Socialite::driver('google')->user();
            dd($user); 
        } catch (Exception $e) {
            Session::flash('error_message', $e->getMessage()); 
            return redirect()->route('admin.auth.login.index'); 
        }
	}
}