<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\Storage; 

class Post extends Model
{
	/**
	 * Table Name 
	 * 
	 * @var string
	 */
	protected $table = 'posts'; 

	/**
	 * Primary Key 
	 * 
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Appends
	 * 
	 * @var array 
	 */
	public $appends = ['banner_url', 'banner_thumb_url', 'banner_small_url']; 

	/**
	 * Get Banner Url 
	 * 
	 * @return string 
	 */
	public function getBannerUrlAttribute()
	{
		if ($this->banner) {
			return Storage::disk('public')->url($this->banner); 
		}
	}

	/**
	 * Get Banner Thumbnail Url 
	 * 
	 * @return string 
	 */
	public function getBannerThumbUrlAttribute()
	{
		if ($this->banner) {
			return Storage::disk('public')->url('thumbnail-'.$this->banner); 
		}
	}

	/**
	 * Get Banner Small Url 
	 * 
	 * @return string 
	 */
	public function getBannerSmallUrlAttribute()
	{
		if ($this->banner) {
			return Storage::disk('public')->url('small-'.$this->banner); 
		}
	}

	/**
	 * Author 
	 * 
	 * @return Eloquent
	 */
	public function author()
	{
		return $this->belongsTo(User::class, 'user_id'); 
	}

	/**
	 * Category 
	 * 
	 * @return Eloquent 
	 */
	public function category()
	{
		return $this->belongsTo(Category::class, 'category_id'); 
	}

	/**
	 * Fillable Mask Assignment 
	 * 
	 * @var array 
	 */
	public $fillable = [
		'category_id', 
		'user_id', 
		'title', 
		'slug', 
		'content', 
		'banner', 
		'status', 
	]; 
}