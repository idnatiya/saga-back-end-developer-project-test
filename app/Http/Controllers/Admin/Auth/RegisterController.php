<?php 

namespace App\Http\Controllers\Admin\Auth; 

use App\Http\Controllers\Admin\Controller as BaseController; 
use App\Http\Requests\Admin\Auth\RegisterRequest;
use App\Services\RegisterService;  
use Illuminate\Foundation\Auth\RegistersUsers; 

class RegisterController extends BaseController
{
	use RegistersUsers; 

	/**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        return view('admin.auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request, RegisterService $registerService)
    {
        $requestData = $request->validated(); 
        $process = $registerService->doRegistration($requestData);

        return response()->json([
            'status' => $process['status'], 
            'message' => $process['message'], 
        ], $process['responseCode']);  
    }
}