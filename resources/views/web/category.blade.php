@extends('web::layouts.index')

@section('title', $category['name'])

@section('content')
    <section class="home">
        <div class="container">
            <div class="row">
                <div class="col-md-8 text-left">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="page-title">Category: {{ $category['name'] }}</h1>
                            <p class="page-subtitle">Showing all posts with category <i>{{ $category['name'] }}</i></p>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="row">
                        @foreach ($posts['data'] as $post)
                            @include('web::components.post', ['post' => $post])
                        @endforeach 
                    </div>
                </div>
                <div class="col-xs-6 col-md-4 sidebar" id="sidebar">
                    @include('web::partials.sidebar')
                </div>
            </div>
        </div>
    </section>
@endsection