<?php

namespace App\Http\Requests\Admin\Post;

use App\Repositories\PostRepository; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    protected $postRepository; 

    /**
     * Construct 
     * 
     * @param PostRepository $postRepository 
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository; 
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $post = $this->postRepository->find($this->id); 
        if ($post == null) {
            abort(404); 
        }

        if (Auth::user()->hasRole('Author')) {
            if ($post['user_id'] !== Auth::user()->id) {
                abort(403); 
            }
        }

        $this->merge([
            'post' => $post, 
        ]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
