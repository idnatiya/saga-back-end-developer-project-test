<article class="col-md-12 article-list">
    <div class="inner">
        <figure>
            <a href="single.html">
                <img src="{{ $post['banner_small_url'] }}">
            </a>
        </figure>
        <div class="details">
            <div class="detail">
                <div class="category">
                    <a href="{{ route('category', ['slug' => $post['category']['slug']]) }}">{{ $post['category']['name'] }}</a>
                </div>
                <div class="time">{{ date('F d, Y', strtotime($post['created_at'])) }}</div>
            </div>
            <h1><a href="{{ route('post', ['slug' => $post['slug']]) }}">{{ $post['title'] }}</a></h1>
            <p>
                {{ Str::words(strip_tags($post['content']), 25, '...') }}
            </p>
            <footer>
                <a href="#" class="love"><i class="ion-android-favorite-outline"></i> <div>{{ $post['hits'] }}</div></a>
                <a class="btn btn-primary more" href="{{ route('post', ['slug' => $post['slug']]) }}">
                    <div>More</div>
                    <div><i class="ion-ios-arrow-thin-right"></i></div>
                </a>
            </footer>
        </div>
    </div>
</article>