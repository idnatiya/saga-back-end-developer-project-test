@extends('web::layouts.index')

@section('title', config('app.name'))

@section('content')
    <section class="home">
        <div class="container">
            <div class="row">
                <div class="col-md-8 text-left">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="page-title">Lastest Posts</h1>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="row">
                        @foreach ($lastestPost as $post)
                            @include('web::components.post', ['post' => $post])
                        @endforeach 
                    </div>
                </div>
                <div class="col-xs-6 col-md-4 sidebar" id="sidebar">
                    @include('web::partials.sidebar')
                </div>
            </div>
        </div>
    </section>
@endsection