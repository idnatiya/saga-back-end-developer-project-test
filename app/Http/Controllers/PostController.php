<?php 

namespace App\Http\Controllers; 

use App\Repositories\PostRepository;

class PostController extends Controller
{
    protected $categoryRepository; 

    /**
     * Constructs
     */
    public function __construct(
        PostRepository $postRepository
    )
    {
        $this->postRepository = $postRepository; 
    }

    /**
     * Index
     */
    public function index($slug)
    {
        $post = $this->postRepository->getWhere('slug', $slug);
        if ( ! $post) {
            abort(404); 
        }

        return view('web::post', [
            'post' => $post, 
        ]); 
    }
}