<?php

namespace App\Http\Requests\Admin\Post;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|max:255', 
            'category_id'   => 'required|exists:categories,id', 
            'banner'        => 'required|mimes:jpg,png|max:2000', 
            'content'       => 'required', 
            'status'        => 'required',
        ];
    }
}
