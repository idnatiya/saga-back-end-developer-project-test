<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model; 

class Category extends Model
{
	/**
	 * Table Name 
	 * 
	 * @var string
	 */
	protected $table = 'categories'; 

	/**
	 * Primary Key 
	 * 
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Fillable Mask Assignment 
	 * 
	 * @var array 
	 */
	public $fillable = ['name', 'slug']; 
}