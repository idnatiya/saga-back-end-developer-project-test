@extends('admin::auth.layout')

@section('title', 'Login | ' . config('app.name'))

@section('content')
	<div class="card card-primary">
		<div class="card-header"><h4>Login</h4></div>
		<div class="card-body">
			<form method="POST" action="{{ route('admin.auth.login.process') }}" onsubmit="doRegistration(this, event)">
				@csrf
				<div class="form-group">
					<div class="d-block">
						<label>Email</label>
					</div>
					<input id="email" type="text" class="form-control" name="email" autocomplete="off" tabindex="1" autofocus>
				</div>
				<div class="form-group">
					<div class="d-block">
						<div class="d-block">
							<label>Password</label>
						</div>
					</div>
					<input id="password" type="password" class="form-control" name="password" tabindex="2">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
						Login
					</button>
				</div>
			</form>
			<div class="text-center mt-4 mb-3">
				<div class="text-job text-muted">Login With Social</div>
			</div>
			<div class="row sm-gutters">
				<div class="col-6">
					<a href="{{ route('admin.auth.socials.redirect', ['platform' => 'google']) }}" class="btn btn-block btn-social btn-danger">
						<span class="fab fa-google"></span> Google
					</a>
				</div>
				<div class="col-6">
					<a href="{{ route('admin.auth.socials.redirect', ['platform' => 'facebook']) }}" class="btn btn-block btn-social btn-facebook">
						<span class="fab fa-facebook"></span> Facebook
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="mt-5 text-muted text-center">
		Don't have an account? <a href="{{ route('admin.auth.register.index') }}">Create One</a>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript">

		/**
		 * Do Loading State 
		 */
		const showLoadingState = (status) => {
			let button = $('button[type="submit"]'); 
			if (status) {
				button.addClass('disabled')
					.attr('disabled', 'disabled')
					.html('Sending data, please wait a moment ... '); 
			} else {
				button.removeClass('disabled')
					.removeAttr('disabled')
					.html('Register'); 
			}
		};

		/**
		 * Do Registration
		 */
		const doRegistration = (element, event) => {
			event.preventDefault(); 
			showLoadingState(true); 
			$element = $(element); 
			axios({
				url: `${window.baseURL}/admin/auth/login`, 
				method: 'POST', 
				data: $element.serialize()
			})
			.then(responseJson => {
				showLoadingState(false); 
				document.location.href = `${window.baseURL}/admin`; 
			})
			.catch(errorResponse => {
				handleErrorRequest(errorResponse); 
				showLoadingState(false); 
			}); 
		}; 

		/**
		 * Handle Error Request 
		 */
		const handleErrorRequest = (errorResponse) => {
			response = errorResponse.response; 
			if (response.status === 422) {
				errors = response.data.errors; 
				$.each(errors, (_, error) => {
					Swal.fire('Warning', error[0], 'warning'); 
					return false; 
				}); 
			} else {
				data = response.data; 
				Swal.fire('Error', data.message, 'error'); 
				return false; 
			}
		}; 

	</script>
@endpush