@extends('admin::layouts.mainapp')

@section('title', 'My Profile')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>My Profile</h1>
        </div>
        <div class="section-body">
            <div class="card profile-widget mt-5">
                <div class="profile-widget-header">
                    <img alt="image" src="{{ Auth::user()->avatar_url }}" class="rounded-circle profile-widget-picture">
                    <div class="profile-widget-items">
                        <div class="profile-widget-item">
                            <div class="profile-widget-item-label">Posts</div>
                            <div class="profile-widget-item-value">{{ Auth::user()->posts->count() }}</div>
                        </div>
                        <div class="profile-widget-item">
                        </div>
                        <div class="profile-widget-item">
                        </div>
                    </div>
                </div>
                <div class="profile-widget-description pb-0">
                    <div class="profile-widget-name">{{ Auth::user()->name }} <div class="text-muted d-inline font-weight-normal"><div class="slash"></div> {{ Auth::user()->roles[0]->name }}</div></div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-sm table-striped">
                                <tr>
                                    <th style="width:300px;">Email</th>
                                    <td> : {{ Auth::user()->email }}</td>
                                </tr>
                                <tr>
                                    <th>Registration Date</th>
                                    <td> : {{ date('F d, Y', strtotime(Auth::user()->created_at)) }}</td>
                                </tr>
                                <tr>
                                    <th>Connected Social Media</th>
                                    <td>
                                        :
                                        @if (Auth::user()->google_id)
                                            <span class="badge badge-danger">Google</span>
                                        @endif
                                        @if (Auth::user()->facebook_id)
                                            <span class="badge badge-primary">Facebook</span>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>    
                </div>
            </div>
            <div class="card">
                <form action="{{ route('admin.profile.update') }}" method="POST" onsubmit="doSubmit(this, event)">
                    @method('PUT')
                    @csrf
                    <div class="card-header">
                        <strong>Change Account Password</strong>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="">Old Password</label>
                            <input type="password" name="old_password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">New Password</label>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Password Confirmation</label>
                            <input type="password" name="password_confirmation" class="form-control">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-danger"> Reset Password </button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        
        /**
		 * Submit Action
		 */
		const doSubmit = (element, event) => {
			event.preventDefault(); 
			$element = $(element); 
			$element.find('button[type="submit"]')
				.addClass('disabled')
				.attr('disabled', 'disabled')
				.html(`<i class="fa fw-fw fa-circle-notch fa-spin"></i> Sending data`); 

			axios({
				url: $element.attr('action'),
				method: $element.attr('method'), 
				data: $element.serialize(), 
			})
			.then(resultJson => {
				Swal.fire('Success', resultJson.data.message, 'success'); 
				$element.find('button[type="submit"]')
					.removeClass('disabled')
					.removeAttr('disabled')
					.html(`Save`); 
				resetInput().then(() => {
					modalForm.modal('hide'); 
				});
			})
			.catch(errorResponse => {
				handleErrorRequest(errorResponse);
				$element.find('button[type="submit"]')
					.removeClass('disabled')
					.removeAttr('disabled')
					.html(`Save`); 
			}); 
		}; 

    </script>
@endpush