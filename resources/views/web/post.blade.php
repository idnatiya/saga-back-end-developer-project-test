@extends('web::layouts.index')

@section('title', $post['title'])

@section('content')
    <section class="single first">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li class="active">{{ $post['category']['name'] }}</li>
                    </ol>
                    <article class="article main-article">
                        <header>
                            <h1>{{ $post['title'] }}</h1>
                            <ul class="details">
                                <li>Posted on {{ date('F d, Y', strtotime($post['created_at'])) }}</li>
                                <li><a href="{{ route('category', ['slug' => $post['category']['slug']]) }}">{{ $post['category']['name'] }}</a></li>
                                <li>By {{ $post['author']['name'] }}</li>
                            </ul>
                        </header>
                        <div class="main">
                            <div class="featured">
                                <figure>
                                    <img src="{{ $post['banner_url'] }}">
                                </figure>
                            </div>

                            {!! $post['content'] !!}
                        </div>
                    </article>
                </div>
                <div class="col-xs-6 col-md-4 sidebar" id="sidebar">
                    @include('web::partials.sidebar')
                </div>
            </div>    
        </div>
    </section>
@endsection