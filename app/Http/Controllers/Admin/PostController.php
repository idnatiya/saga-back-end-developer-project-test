<?php 

namespace App\Http\Controllers\Admin; 

use App\Repositories\PostRepository; 
use App\Repositories\CategoryRepository; 
use App\Http\Controllers\Admin\Controller as BaseController; 
use App\Http\Requests\Admin\Post\TableRequest; 
use App\Http\Requests\Admin\Post\EditRequest; 
use App\Http\Requests\Admin\Post\StoreRequest; 
use App\Http\Requests\Admin\Post\UpdateRequest; 
use App\Http\Requests\Admin\Post\DeleteRequest; 
use App\Services\PostService; 
use Illuminate\Support\Facades\Auth; 

class PostController extends BaseController
{
	protected $postRepository; 
	protected $postService; 
	protected $categoryRepository; 

	/**
	 * Construct
	 */
	public function __construct(
		PostRepository $postRepository,
		CategoryRepository $categoryRepository, 
		PostService $postService
	)
	{
		$this->postRepository = $postRepository; 
		$this->categoryRepository = $categoryRepository; 
		$this->postService = $postService; 
	}

	/**
	 * Index
	 * 
	 * @return void
	 */
	public function index()
	{
		if (Auth::user()->hasRole('Admin')) {
			return view('admin::post.index-admin'); 
		} else {
			return view('admin::post.index-author'); 
		}
	}

	/**
	 * Create
	 * 
	 * @return void
	 */
	public function create()
	{
		return view('admin::post.create', [
			'categories' => $this->categoryRepository->findAll(), 
		]); 
	}

	/**
	 * Edit 
	 * 
	 * @param  mix $id 
	 * @return void
	 */
	public function edit(EditRequest $requestData)
	{
	 	return view('admin::post.edit', [
	 		'currentPost' => $requestData->post, 
	 		'categories' => $this->categoryRepository->findAll(), 
	 	]); 
	}

	/**
	 * Tables 
	 * 
	 * @return Illuminate\Response\Json
	 */
	public function tables(TableRequest $requestData)
	{
		$requestData = $requestData->validated(); 

		if (Auth::user()->hasRole('Admin')) {
			$dataTables = $this->postRepository->getAdminTable($requestData);
		} else {
			$dataTables = $this->postRepository->getPostByAuthor(Auth::user()->id, $requestData);
		}

		return $dataTables;  
	}

	/**
	 * Store 
	 * 
	 * @param  StoreRequest $requestData 
	 * @return Illuminate\Response\Json                  
	 */
	public function store(StoreRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		$requestData['user_id'] = Auth::user()->id; 

		$process = $this->postService->store($requestData); 

		return response()->json([
			'status' => $process['status'],
			'message' => $process['message'],
		], $process['responseCode']); 
	}

	/**
	 * Update 
	 * 
	 * @param  UpdateRequest $requestData 
	 * @return Illuminate\Response\Json                  
	 */
	public function update(UpdateRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		
		$process = $this->postService->update($requestData['id'], $requestData); 

		return response()->json([
			'status' => $process['status'],
			'message' => $process['message'],
		], $process['responseCode']); 
	}

	/**
	 * Delete 
	 * 
	 * @param  Delete $requestData 
	 * @return Illuminate\Response\Json                  
	 */
	public function delete(DeleteRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		$process = $this->postService->delete($requestData); 

		return response()->json([
			'status' => $process['status'],
			'message' => $process['message'],
		], $process['responseCode']); 
	}
}