<?php 

namespace App\Services; 

use App\Models\User; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str; 
use Laravel\Socialite\Facades\Socialite;

class GoogleAuthService implements AuthServiceInterface
{
	/**
	 * Generate Avatar 
	 * 
	 * @param  string $resourceUrl 
	 * @return string              
	 */
	protected function generateAvatar($resourceUrl)
	{
		$fileName = 'users/' . Str::uuid().'.jpg';
		Storage::disk('public')->put($fileName, file_get_contents($resourceUrl)); 

		return $fileName; 
	}

	/**
	 * Check User Exits
	 * 
	 * @param  object $user
	 * @return App\Models\User
	 */
	protected function checkUserExists($user)
	{
		$currentUser = User::where('email', $user->email)->first(); 

		if ($currentUser) {
			$currentUser->google_id = $user->id; 
			$currentUser->save(); 
		} else {
			$currentUser = User::create([
				'name' 				=> $user->name, 
				'email' 			=> $user->email, 
				'google_id' 		=> $user->id, 
				'email_verified_at' => date('Y-m-d H:i:s'), 
				'password' 			=> '', 
				'avatar' 			=> $this->generateAvatar($user->avatar), 
			]);

			$currentUser->assignRole('Author'); 
		}

		return $currentUser; 
	}

	/**
	 * Handle 
	 * 
	 * @return boolean 
	 */
	public function handle()
	{
		$user = Socialite::driver('google')->user();
		$userExists = $this->checkUserExists($user); 
		Auth::login($userExists); 

		return true; 
	}
}