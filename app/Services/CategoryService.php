<?php 

namespace App\Services; 

use App\Repositories\CategoryRepository; 
use Illuminate\Support\Str; 
use Illuminate\Support\Facades\DB; 

class CategoryService extends BaseService
{
	/**
	 * Construct 
	 * 
	 * @param CategoryRepository $repository 
	 */
	public function __construct(CategoryRepository $repository)	
	{
		$this->repository = $repository; 
	}

	/**
	 * Store 
	 * 
	 * @param  array $requestData 
	 * @return array 
	 */
	public function store($requestData)
	{
		DB::beginTransaction(); 

		try {
			$requestData['slug'] = Str::slug($requestData['name']); 
			$this->repository->create($requestData); 

			DB::commit(); 

			return [
				'status' => 'success', 
				'message' => 'Successfully store data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed', 
				'message' => $e->getMessage(), 
				'responseCode' => 500, 
			]; 
		}
	}

	/**
	 * Update
	 * 
	 * @param  mix $id          
	 * @param  array $requestData 
	 * @return boolean
	 */
	public function update($id, $requestData)
	{
		DB::beginTransaction(); 

		try {
			$requestData['slug'] = Str::slug($requestData['name']); 
			$this->repository->update($id, $requestData); 

			DB::commit(); 

			return [
				'status' => 'success', 
				'message' => 'Successfully update data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed', 
				'message' => $e->getMessage(), 
				'responseCode' => 500, 
			]; 
		}
	}
}