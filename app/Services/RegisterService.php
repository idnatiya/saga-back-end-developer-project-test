<?php 

namespace App\Services; 

use App\Models\User; 
use Illuminate\Support\Facades\DB; 
use Illuminate\Support\Facades\Hash; 
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth; 

class RegisterService
{
	/**
	 * Do Registration 
	 * 
	 * @param  mix $requestData 
	 * @return array             
	 */
	public function doRegistration($requestData)
	{
		DB::beginTransaction();

		try {
			// Hash Password
			$requestData['password'] = Hash::make($requestData['password']); 
			// Store Data 
			$user = User::create($requestData);
			// Assign to Author
			$user->assignRole('Author'); 
			// Send Email Verification
			$user->sendEmailVerificationNotification();
			// Sign In User
			Auth::login($user); 			

			DB::commit(); 

			return [
				'status' => 'success', 
				'responseCode' => 200,
				'message' => 'Successfully Register User', 
			]; 
		} catch (Exception $e) {

			DB::rollback(); 

			return [
				'status' => 'failed', 
				'responseCode' => 500, 
				'message' => $e->getMessage(), 
			]; 
		}
	}
}