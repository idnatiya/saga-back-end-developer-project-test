<aside id="sidebar-wrapper">
	<div class="sidebar-brand">
		<a href="{{ route('admin.index') }}">
			{{ config('app.name') }}
		</a>
	</div>
	<div class="sidebar-brand sidebar-brand-sm">
		<a href="index.html">St</a>
	</div>
	<ul class="sidebar-menu">
		<li class="menu-header">Dashboard</li>
		<li><a class="nav-link" href="{{ route('admin.index') }}"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
		<li><a class="nav-link" href="{{ route('index') }}" target="_blank"><i class="fas fa-globe"></i> <span>Website</span></a></li>
		<li class="menu-header">Post Management</li>
		<li><a class="nav-link" href="{{ route('admin.post.index') }}"><i class="fas fa-pencil-ruler"></i> <span>Posts</span></a></li>
		<li><a class="nav-link" href="{{ route('admin.category.index') }}"><i class="fas fa-th-large"></i> <span>Categories</span></a></li>
		@if (Auth::user()->hasRole('Admin'))
			<li class="menu-header">User Management</li>
			<li><a class="nav-link" href="{{ route('admin.user.index') }}"><i class="fas fa-columns"></i> <span>Users</span></a></li>
		@endif
	</ul>
</aside>