<?php 

namespace App\Http\Controllers; 

use App\Repositories\CategoryRepository; 
use App\Repositories\PostRepository;

class CategoryController extends Controller
{
    protected $categoryRepository; 

    /**
     * Constructs
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        PostRepository $postRepository
    )
    {
        $this->categoryRepository = $categoryRepository; 
        $this->postRepository = $postRepository; 
    }

    /**
     * Index
     */
    public function index($slug)
    {
        $category = $this->categoryRepository->getWhere('slug', $slug);
        if ( ! $category) {
            abort(404); 
        }

        return view('web::category', [
            'category' => $category, 
            'posts' => $this->postRepository->getPostByCategory($category['id']), 
        ]); 
    }
}