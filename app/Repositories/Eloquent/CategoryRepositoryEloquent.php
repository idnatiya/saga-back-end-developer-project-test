<?php 

namespace App\Repositories\Eloquent; 

use App\Models\Category; 
use App\Repositories\CategoryRepository; 
use App\Repositories\Eloquent\BaseRepositoryEloquent; 

class CategoryRepositoryEloquent extends BaseRepositoryEloquent implements CategoryRepository
{
	/**
	 * Construct 
	 * 
	 * @param Category $model 
	 */
	public function __construct(Category $model)
	{
		$this->model = $model; 
	}

	/**
	 * Get CMS Table
	 * 
	 * @param  array $requestData 
	 * @return array              
	 */
	public function getCmsTable($requestData)
	{
		$categoryData = $this->model->query(); 

		if (isset($requestData['keyword'])) {
			$categoryData = $categoryData->where(function ($query) use ($requestData) {
				$query->where('name', 'like', '%'.$requestData['keyword'].'%'); 
			}); 			
		}

		$categoryData = $categoryData->paginate(10); 
		$categoryData = $categoryData->toArray(); 

		return $categoryData;
	}

	/**
	 * Delete
	 * 
	 * @param  mix $id 
	 * @return boolean
	 */
	public function delete($id)
	{
		$category = $this->model->find($id); 
		$category->delete(); 

		return true;
	}
}