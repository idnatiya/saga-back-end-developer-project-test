<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Saga Back End Developer Test Project

## Service Dependencies

1) Laravel Framework - Version 9.2
2) MariaDB - Version 10.4.20
3) Apache - Version 2.4.48
4) PHP - Vesion 8.0.2
5) Email Fake Server - [https://mailtrap.io](https://mailtrap.io), Credentials : 
```bash
Email : andi@bonet.co.id
Password : @B0g0r123
```

## Panduan Instalasi

1) Clone Repository 
```bash
git clone https://gitlab.com/idnatiya/saga-back-end-developer-project-test.git
```

2. Install Paket yang dibutuhkan 
```bash
composer install
```

3) Buat database dengan nama ```backend_devtest_project```

4) Buat file ```.env``` di root directory, copy paste isi dari file ```.env.example```

5) Sesuaikan bagian konfigurasi database sesuai dengan credentials : 
``` bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=backend_devtest_project
DB_USERNAME=root
DB_PASSWORD=
```
6) Jalankan ```Migration``` (struktur data) & Database ```Seeder``` (Isi)
```bash
php artisan migrate --seed
```
7) Buat ```symlink``` dari ```public folder``` ke ```storage folder```
```bash
php artisan storage:link
```

8) Buat Kunci Enskripsi Aplikasi 
```bash
php artisan key:generate
```

9) Jalankan development server
```bash
php artisan serve
```
## Default Email & Password Admin
Email : ```admin@gmail.com```
Password : ```admin@gmail.com```
