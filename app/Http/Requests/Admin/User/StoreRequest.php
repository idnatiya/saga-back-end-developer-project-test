<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => ['required', 'max:255'], 
            'email'                 => ['required', 'email', 'max:255', 'unique:users,email'], 
            'role'                  => 'required|exists:roles,id',
            'password'              => ['required', 'max:255', Password::min(6)->mixedCase()->numbers()->symbols(), 'confirmed'], 
            'password_confirmation' => ['required'], 
        ];
    }
}
