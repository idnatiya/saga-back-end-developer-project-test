<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 3,
                'name' => 'My Indonesia',
                'slug' => 'my-indonesia',
                'created_at' => '2022-04-09 15:45:06',
                'updated_at' => '2022-04-09 15:45:06',
            ),
            1 => 
            array (
                'id' => 4,
                'name' => 'Motivation',
                'slug' => 'motivation',
                'created_at' => '2022-04-09 15:45:13',
                'updated_at' => '2022-04-09 15:45:13',
            ),
            2 => 
            array (
                'id' => 5,
                'name' => 'Relationship',
                'slug' => 'relationship',
                'created_at' => '2022-04-09 15:45:21',
                'updated_at' => '2022-04-09 15:45:21',
            ),
            3 => 
            array (
                'id' => 6,
                'name' => 'Success',
                'slug' => 'success',
                'created_at' => '2022-04-09 15:45:29',
                'updated_at' => '2022-04-09 15:45:29',
            ),
        ));
        
        
    }
}