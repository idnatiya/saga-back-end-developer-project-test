@extends('admin::layouts.mainapp')

@section('title', 'Email Verification')

@section('content')
	<section class="section">
		<div class="section-header">
			<h1>Email Verification</h1>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-body">
							<div class="empty-state" data-height="400" style="height: 400px;">
								<div class="empty-state-icon">
									<i class="fas fa-user"></i>
								</div>
								<h2>Verify Your Email Address</h2>
								@if (session('resent'))
									<div class="alert alert-success" role="alert">
										{{ __('A fresh verification link has been sent to your email address.') }}
									</div>
								@endif
								<p class="lead mb-0">
									Before proceeding, please check your email for a verification link.
								</p>
								<p class="text-muted m-1">
									If you did not receive the email
								</p>
								<a href="{{ route('admin.auth.verification.resend') }}" class="btn btn-primary btn-resend mt-4">Click here to request another</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('scripts')
	<script type="text/javascript">
		
		/**
		 * Resend
		 */
		$('.btn-resend').on('click', function (e) {
			e.preventDefault(); 
			$(this).addClass('disabled')
				.html(`<i class="fa f-fw fa-circle-notch fa-spin"></i> Please wait .... `); 
			document.location.href = $(this).attr('href'); 
		});

	</script>
@endpush