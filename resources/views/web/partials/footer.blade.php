<!-- Start footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright mt-0">
                    COPYRIGHT &copy;  2022. ALL RIGHT RESERVED.
                    <div>
                        Template Powered <i class="ion-heart"></i> by <a href="http://kodinger.com" target="_blank">MAGZ</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->