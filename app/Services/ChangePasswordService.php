<?php 

namespace App\Services; 

use App\Repositories\UserRepository; 
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\DB; 

class ChangePasswordService
{
    protected $userRepository; 

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository; 
    }

    public function change($requestData)
    {
        DB::beginTransaction(); 

        try {

            if (Hash::check($requestData['old_password'], $requestData['old_hashed_password']) == FALSE) {
                return [
                    'status'        => 'failed',
                    'message'       => 'Old password is incorrect', 
                    'responseCode'  => 500,
                ]; 
            }

            $this->userRepository->update($requestData['user_id'], [
                'password' => Hash::make($requestData['password']), 
            ]); 

            DB::commit(); 

            return [
                'status'        => 'success', 
                'message'       => 'Successfully update password', 
                'responseCode'  => 200,
            ]; 

        } catch (Exception $e) {
            DB::rollback(); 
            return [
                'status'        => 'failed',
                'message'       => $e->getMessage(), 
                'responseCode'  => 500,
            ]; 
        }
    }
}