<?php 

namespace App\Services; 

use Illuminate\Support\Facades\DB; 

class BaseService
{
	/**
	 * Repository
	 */
	protected $repository; 

	/**
	 * Store 
	 * 
	 * @param  array $requestData 
	 * @return array 
	 */
	public function store($requestData)
	{
		DB::beginTransaction(); 

		try {
			$this->repository->create($requestData); 

			DB::commit(); 

			return [
				'status' => 'success', 
				'message' => 'Successfully store data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed', 
				'message' => $e->getMessage(), 
				'responseCode' => 500, 
			]; 
		}
	}

	/**
	 * Update
	 * 
	 * @param  mix $id          
	 * @param  array $requestData 
	 * @return boolean
	 */
	public function update($id, $requestData)
	{
		DB::beginTransaction(); 

		try {
			$this->repository->update($id, $requestData); 

			DB::commit(); 

			return [
				'status' => 'success', 
				'message' => 'Successfully update data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed', 
				'message' => $e->getMessage(), 
				'responseCode' => 500, 
			]; 
		}
	}

	/**
	 * Delete
	 * 
	 * @param  array $id 
	 * @return boolean
	 */
	public function delete($requestData)
	{
		DB::beginTransaction(); 

		try {
			foreach ($requestData['id'] as $id) {
				$this->repository->delete($id); 
			}

			DB::commit(); 

			return [
				'status' => 'success', 
				'message' => 'Successfully delete data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed', 
				'message' => $e->getMessage(), 
				'responseCode' => 500, 
			]; 
		}
	}
}