<header class="primary">
	<div class="firstbar">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="brand">
						<a href="{{ route('index') }}">
							<img src="{{ asset('assets-web/images/logo.png') }}" alt="Magz Logo">
						</a>
					</div>						
				</div>
				<div class="col-md-9 col-sm-12 text-right">
					<ul class="nav-icons">
						@if (Auth::check()) 
							<li><a href="{{ route('admin.index') }}"><i class="ion-person"></i><div>{{ Auth::user()->name }}</div></a></li>
							<li><a href="{{ route('admin.auth.logout') }}"><i class="ion-log-out"></i><div>Log out</div></a></li>
						@else
							<li><a href="{{ route('admin.auth.register.index') }}"><i class="ion-person-add"></i><div>Register</div></a></li>
							<li><a href="{{ route('admin.auth.login.index') }}"><i class="ion-person"></i><div>Login</div></a></li>
						@endif
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!-- Start nav -->
	<nav class="menu">
		<div class="container">
			<div class="brand">
				<a href="#">
					<img src="{{ asset('assets-web/images/logo.png') }}" alt="Magz Logo">
				</a>
			</div>
			<div class="mobile-toggle">
				<a href="#" data-toggle="menu" data-target="#menu-list"><i class="ion-navicon-round"></i></a>
			</div>
			<div class="mobile-toggle">
				<a href="#" data-toggle="sidebar" data-target="#sidebar"><i class="ion-ios-arrow-left"></i></a>
			</div>
			<div id="menu-list">
				<ul class="nav-list">
					<li class="for-tablet nav-title"><a>Menu</a></li>
					@if (Auth::check()) 
						<li class="for-tablet"><a href="{{ route('admin.index') }}">{{ Auth::user()->name }}</a></li>
						<li class="for-tablet"><a href="{{ route('admin.auth.logout') }}">Log Out</a></li>
					@else
						<li class="for-tablet"><a href="{{ route('admin.auth.login.index') }}">Login</a></li>
						<li class="for-tablet"><a href="{{ route('admin.auth.register.index') }}">Register</a></li>
					@endif
					<li><a href="{{ route('index') }}">Home</a></li>
					@foreach (\Facades\App\Repositories\CategoryRepository::findAll() as $category)
						<li><a href="{{ route('category', ['slug' => $category['slug']]) }}">{{ $category['name'] }}</a></li>
					@endforeach 
				</ul>
			</div>
		</div>
	</nav>
	<!-- End nav -->
</header>