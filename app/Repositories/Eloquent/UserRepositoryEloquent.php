<?php 

namespace App\Repositories\Eloquent; 

use App\Models\User; 
use App\Repositories\UserRepository; 
use App\Repositories\Eloquent\BaseRepositoryEloquent; 

class UserRepositoryEloquent extends BaseRepositoryEloquent implements UserRepository
{
	/**
	 * Construct 
	 * 
	 * @param User $model 
	 */
	public function __construct(User $model)
	{
		$this->model = $model; 
	}

	/**
	 * Get CMS Table
	 * 
	 * @param  array $requestData 
	 * @return array              
	 */
	public function getCmsTable($requestData)
	{
		$userData = $this->model->query(); 
		$userData = $userData->with(['roles']); 

		if (isset($requestData['keyword'])) {
			$userData = $userData->where(function ($query) use ($requestData) {
				$query->where('name', 'like', '%'.$requestData['keyword'].'%'); 
				$query->orWhere('email', 'like', '%'.$requestData['keyword'].'%');
			}); 			
		}

		$userData = $userData->paginate(10); 
		$userData = $userData->toArray(); 

		return $userData;
	}

	/**
	 * Delete
	 * 
	 * @param  mix $id 
	 * @return boolean
	 */
	public function delete($id)
	{
		$user = $this->model->find($id); 
		$user->delete(); 

		return true;
	}
}