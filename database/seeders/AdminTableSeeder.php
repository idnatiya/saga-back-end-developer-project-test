<?php

namespace Database\Seeders;

use App\Models\User; 
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email = 'admin@gmail.com'; 
        $check = User::where('email', $email)->first(); 

        if ( ! $check) {
            $user = User::create([
                'name' => 'Admin', 
                'email' => $email, 
                'password' => Hash::make($email), 
                'email_verified_at' => date('Y-m-d H:i:s'),
            ]); 

            $user->assignRole('Admin'); 
        }
    }
}
