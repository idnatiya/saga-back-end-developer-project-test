/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */
/**
 * Handle Error Request 
 */
const handleErrorRequest = (errorResponse) => {
    response = errorResponse.response; 
    if (response.status === 422) {
        errors = response.data.errors; 
        $.each(errors, (_, error) => {
            Swal.fire('Warning', error[0], 'warning'); 
            return false; 
        }); 
    } else {
        data = response.data; 
        Swal.fire('Error', data.message, 'error'); 
        return false; 
    }
}; 

window.showLoadingSwal = () => {
    Swal.fire({
        title: 'Please Wait',
        imageUrl: `${window.baseURL}/assets-admin/img/loading_.gif`,
        allowOutsideClick: false,
        showCancelButton: false,
        showConfirmButton: false
    })
}

const actionPagination = (element, event, table) => {
    event.preventDefault(); 
    table.baseURL = element.getAttribute('href');
    table.loadDataTables();  
}; 

const renderPagination = (links, $container, table) => {
    linksHtml = `<ul class="pagination pagination-sm mb-0">`;
    links.forEach(item => {
        linksHtml += `<li class="page-item ${item.active ? 'active' : ''} ${item.url == null ? 'disabled' : ''}">
            <a class="page-link" href="${item.url}" onclick="actionPagination(this, event, table)">${item.label}</a>
        </li>`; 
    }); 
    linksHtml += '</ul>'; 

    $container.html(linksHtml);
}; 

const showDeleteConfirmation = () => {
    return new Promise((resolve, reject) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            return resolve({
                status: result.isConfirmed
            }); 
        })
    }); 
}; 