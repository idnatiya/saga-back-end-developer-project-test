<?php 

namespace App\Repositories\Eloquent; 

class BaseRepositoryEloquent 
{
	/**
	 * Model
	 */
	protected $model; 

	/**
	 * Find All 
	 * 
	 * @return array 
	 */
	public function findAll()
	{
		return $this->model->all()->toArray(); 
	}

	/**
	 * Count 
	 * 
	 * @return integer
	 */
	public function count()
	{
		return $this->model->count(); 
	}

	/**
	 * Get Where 
	 * 
	 * @param  mix $column 
	 * @param  mix $value  
	 * @return mix
	 */
	public function getWhere($column, $value)
	{
		$data = $this->model->query(); 
		$data = $data->where($column, $value); 
		$data = $data->first(); 

		if ($data) {
			return $data->toArray(); 
		}

		return null;
	}

	/**
	 * Find 
	 * 
	 * @param  mix $id 
	 * @return array 
	 */
	public function find($id)
	{
		$data = $this->model->find($id); 
		if (is_null($data)) {
			return null; 
		}

		return $data->toArray(); 
	}

	/**
	 * Create
	 *
	 * @param  array 	
	 * @return array
	 */
	public function create($data)
	{
		return $this->model->create($data)->toArray(); 
	}

	/**
	 * Update 
	 * 
	 * @param  mix 		$id   
	 * @param  array 	$data 
	 * @return boolean       
	 */
	public function update($id, $data)
	{
		return $this->model->where($this->model->getKeyName(), $id)->update($data);
	}

	/**
	 * Delete 
	 * 
	 * @param  mix $id 
	 * @return boolean
	 */
	public function delete($id)
	{
		return $this->model->where($this->model->getKeyName(), $id)->delete();
	}
}