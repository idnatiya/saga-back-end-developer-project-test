<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('auth.')->prefix('auth')->group(function () {
    Route::name('login.')->prefix('login')->group(function () {
        Route::get('/', 'Auth\LoginController@showLoginForm')->name('index'); 
        Route::post('/', 'Auth\LoginController@login')->name('process');
    });

    Route::name('socials.')->prefix('socials')->group(function () {
        Route::get('/{platform}', 'Auth\LoginController@socialRedirect')->name('redirect'); 
        Route::get('/{platform}/callback', 'Auth\LoginController@socialCallback')->name('callback'); 
    }); 

    Route::name('register.')->prefix('register')->group(function () {
        Route::get('/', 'Auth\RegisterController@showRegistrationForm')->name('index'); 
        Route::post('/', 'Auth\RegisterController@register')->name('process'); 
    }); 

    Route::name('verification.')->prefix('verification')->group(function () {
        Route::get('/verify', 'Auth\VerificationController@verify')->name('verify')->middleware('auth'); 
        Route::get('/notice', 'Auth\VerificationController@show')->name('notice')->middleware('auth'); 
        Route::get('/resend', 'Auth\VerificationController@resend')->name('resend')->middleware('auth'); 
    }); 

    Route::name('logout')->get('logout', 'Auth\LoginController@logout')->middleware('auth'); 
}); 

Route::middleware('auth')->group(function () {
    Route::middleware('verified')->group(function () {
        Route::get('/', 'ProfileController@index')->name('index'); 

        Route::name('profile.')->prefix('profile')->group(function () {
            Route::get('/', 'ProfileController@index')->name('index'); 
            Route::put('/', 'ProfileController@update')->name('update'); 
        }); 

        Route::name('post.')->prefix('post')->group(function () {
            Route::get('/', 'PostController@index')->name('index'); 
            Route::get('/create', 'PostController@create')->name('create');
            Route::get('/{id}/edit', 'PostController@edit')->name('edit'); 
            Route::get('/tables', 'PostController@tables')->name('tables'); 
            Route::post('/', 'PostController@store')->name('store'); 
            Route::put('/', 'PostController@update')->name('update'); 
            Route::delete('/', 'PostController@delete')->name('delete'); 
        }); 
    
        Route::name('category.')->prefix('category')->group(function () {
            Route::get('/', 'CategoryController@index')->name('index'); 
            Route::get('/tables', 'CategoryController@tables')->name('tables'); 
            Route::post('/', 'CategoryController@store')->name('store'); 
            Route::put('/', 'CategoryController@update')->name('update'); 
            Route::delete('/', 'CategoryController@delete')->name('delete'); 
        }); 
        
        Route::middleware('role:Admin')->name('user.')->prefix('user')->group(function () {
            Route::get('/', 'UserController@index')->name('index'); 
            Route::get('/tables', 'UserController@tables')->name('tables'); 
            Route::post('/', 'UserController@store')->name('store'); 
            Route::put('/', 'UserController@update')->name('update'); 
            Route::delete('/', 'UserController@delete')->name('delete'); 
        }); 
    }); 
}); 