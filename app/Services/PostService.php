<?php 

namespace App\Services; 

use App\Repositories\PostRepository; 
use Illuminate\Support\Str; 
use Illuminate\Support\Facades\DB; 
use Intervention\Image\Facades\Image; 
use Illuminate\Support\Facades\Storage; 

class PostService extends BaseService
{
	/**
	 * Construct 
	 * 
	 * @param PostRepository $repository 
	 */
	public function __construct(PostRepository $repository)	
	{
		$this->repository = $repository; 
	}

	/**
	 * Upload Image 
	 * 
	 * @param  mix $image 
	 * @return string
	 */
	protected function uploadImage($image)
	{
		$fileName = Str::uuid().'.jpg';

		$image = Image::make($image); 
		
		// Make Thumnail 
		$thumbnail = $image->resize(300, null, function ($constraint) {
		    $constraint->aspectRatio();
		}); 
		Storage::disk('public')->put('thumbnail-posts/'.$fileName, $thumbnail->encode()); 

		// Make Small Image 
		$small = $image->resize(600, null, function ($constraint) {
		    $constraint->aspectRatio();
		}); 
		Storage::disk('public')->put('small-posts/'.$fileName, $small->encode()); 

		// Original
		Storage::disk('public')->put('posts/'.$fileName, $image->encode()); 

		return 'posts/'.$fileName;
	}	

	/**
	 * Store 
	 * 
	 * @param  array $requestData 
	 * @return array 
	 */
	public function store($requestData)
	{
		DB::beginTransaction(); 

		try {
			$requestData['slug'] = Str::slug($requestData['title']); 
			$requestData['banner'] = $this->uploadImage($requestData['banner']); 
			$this->repository->create($requestData); 

			DB::commit(); 

			return [
				'status' => 'success', 
				'message' => 'Successfully store data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed', 
				'message' => $e->getMessage(), 
				'responseCode' => 500, 
			]; 
		}
	}

	/**
	 * Update
	 * 
	 * @param  mix $id          
	 * @param  array $requestData 
	 * @return boolean
	 */
	public function update($id, $requestData)
	{
		DB::beginTransaction(); 

		try {
			$requestData['slug'] = Str::slug($requestData['title']); 

			if (isset($requestData['banner'])) {
				$requestData['banner'] = $this->uploadImage($requestData['banner']);
				if (Storage::disk('public')->exists($requestData['post']['banner'])) {
					Storage::disk('public')->delete($requestData['post']['banner']); 
				} 
			}
			
			unset($requestData['post']);

			$this->repository->update($id, $requestData); 

			DB::commit(); 

			return [
				'status' => 'success', 
				'message' => 'Successfully update data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed', 
				'message' => $e->getMessage(), 
				'responseCode' => 500, 
			]; 
		}
	}

	/**
	 * Delete
	 * 
	 * @param  array $id 
	 * @return boolean
	 */
	public function delete($requestData)
	{
		DB::beginTransaction(); 

		try {
			foreach ($requestData['id'] as $id) {
				$this->repository->delete($id); 
			}

			foreach ($requestData['deletedImages'] as $image) {
				if (Storage::disk('public')->exists($image)) {
					Storage::disk('public')->delete($image); 
				}
			}

			DB::commit(); 

			return [
				'status' => 'success', 
				'message' => 'Successfully delete data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed', 
				'message' => $e->getMessage(), 
				'responseCode' => 500, 
			]; 
		}
	}
}