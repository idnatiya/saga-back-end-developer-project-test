<?php 

namespace App\Http\Controllers\Admin; 

use App\Repositories\CategoryRepository; 
use App\Repositories\RoleRepository; 
use App\Http\Controllers\Admin\Controller as BaseController; 
use App\Http\Requests\Admin\Category\TableRequest; 
use App\Http\Requests\Admin\Category\StoreRequest; 
use App\Http\Requests\Admin\Category\UpdateRequest; 
use App\Http\Requests\Admin\Category\DeleteRequest; 
use App\Services\CategoryService; 

class CategoryController extends BaseController
{
	protected $categoryRepository; 
	protected $categoryService; 

	/**
	 * Construct
	 */
	public function __construct(
		CategoryRepository $categoryRepository,
		CategoryService $categoryService
	)
	{
		$this->categoryRepository = $categoryRepository; 
		$this->categoryService = $categoryService; 
	}

	/**
	 * Index
	 * 
	 * @return void
	 */
	public function index()
	{
		return view('admin::category.index'); 
	}

	/**
	 * Tables 
	 * 
	 * @return Illuminate\Response\Json
	 */
	public function tables(TableRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		$dataTables = $this->categoryRepository->getCmsTable($requestData);

		return $dataTables;  
	}

	/**
	 * Store 
	 * 
	 * @param  StoreRequest $requestData 
	 * @return Illuminate\Response\Json                  
	 */
	public function store(StoreRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		$process = $this->categoryService->store($requestData); 

		return response()->json([
			'status' => $process['status'],
			'message' => $process['message'],
		], $process['responseCode']); 
	}

	/**
	 * Update 
	 * 
	 * @param  UpdateRequest $requestData 
	 * @return Illuminate\Response\Json                  
	 */
	public function update(UpdateRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		$process = $this->categoryService->update($requestData['id'], $requestData); 

		return response()->json([
			'status' => $process['status'],
			'message' => $process['message'],
		], $process['responseCode']); 
	}

	/**
	 * Delete 
	 * 
	 * @param  Delete $requestData 
	 * @return Illuminate\Response\Json                  
	 */
	public function delete(DeleteRequest $requestData)
	{
		$requestData = $requestData->validated(); 
		$process = $this->categoryService->delete($requestData); 

		return response()->json([
			'status' => $process['status'],
			'message' => $process['message'],
		], $process['responseCode']); 
	}
}