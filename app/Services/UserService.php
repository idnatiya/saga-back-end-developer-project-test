<?php 

namespace App\Services; 

use App\Models\User; 
use App\Models\Role; 
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\DB; 
use App\Repositories\UserRepository; 
use App\Repositories\RoleRepository; 

class UserService
{
	protected $userRepository; 
	protected $roleRepository; 

	/**
	 * Construct 
	 * 
	 * @param UserRepository $userRepository 
	 * @param RoleRepository $roleRepository 
	 */
	public function __construct(
		UserRepository $userRepository, 
		RoleRepository $roleRepository
	)
	{	
		$this->userRepository = $userRepository; 
		$this->roleRepository = $roleRepository; 
	}	

	/**
	 * Do Store
	 * 
	 * @param  array $requestData 
	 * @return array              
	 */
	public function doStore($requestData)
	{
		DB::beginTransaction(); 

		try {
			$requestData['password'] = Hash::make($requestData['password']); 
			$user = User::create($requestData); 
			$role = Role::find($requestData['role']); 
			$user->assignRole($role->name); 

			DB::commit(); 

			return [
				'status' => 'success',
				'message' => 'Successfully create data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed',
				'message' => $e->getMessage(), 
				'responseCode' => 500, 
			]; 
		}; 
	}

	/**
	 * Do Update
	 * 
	 * @param  array $requestData 
	 * @return array              
	 */
	public function doUpdate($requestData)
	{
		DB::beginTransaction(); 

		try {
			if ($requestData['password']) {
				$requestData['password'] = Hash::make($requestData['password']);
			} else {
				unset($requestData['password']); 
			}

			$role = Role::find($requestData['role']); 

			$user = User::find($requestData['id']); 

			$user->update($requestData);

			$user->syncRoles($role->name); 

			DB::commit(); 

			return [
				'status' => 'success',
				'message' => 'Successfully create data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed', 
				'message' => $e->getMessage(), 
				'responseCode' => 500
			]; 
		}
	}

		
	/**
	 * Delete
	 * @param  array $requestData 
	 * @return array              
	 */
	public function doDelete($requestData)
	{
		DB::beginTransaction(); 

		try {
			foreach ($requestData['id'] as $id) {
				$this->userRepository->delete($id); 
			}

			DB::commit(); 

			return [
				'status' => 'success',
				'message' => 'Successfully create data', 
				'responseCode' => 200, 
			]; 
		} catch (Exception $e) {
			DB::rollback(); 
			return [
				'status' => 'failed', 
				'message' => $e->getMessage(), 
				'responseCode' => 500
			]; 
		}
	}
}