@extends('admin::layouts.mainapp')

@section('title', 'List All Post')

@section('content')
	<div class="section">
		<div class="section-header d-flex justify-content-between">
			<h1>Edit Post</h1>
		</div>
		<div class="section-body">
			<div class="card">
				<form action="{{ route('admin.post.update') }}" method="POST" onsubmit="store(this, event)">
					@csrf
					@method('PUT')
					<input type="hidden" name="id" value="{{ $currentPost['id'] }}">
					<div class="card-header">
						<a href="{{ route('admin.post.index') }}" class="btn btn-success btn-sm"> &larr; back </a>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label>Title</label>
							<input type="text" name="title" value="{{ $currentPost['title'] }}" class="form-control" placeholder="Enter Post Title">
						</div>
						<div class="form-group">
							<label>Category</label>
							<select name="category_id" class="form-control">
								<option value="">Select Category</option>
								@foreach ($categories as $category)
									<option value="{{ $category['id'] }}" {{ ($currentPost['category_id'] == $category['id']) ? 'selected' : '' }}>{{ $category['name'] }}</option>
								@endforeach 
							</select>
						</div>
						<div class="form-group">
							<label>Banner</label>
							<div class="upload-image">
								<button type="button" class="btn btn-sm btn-primary">
									<i class="fa fa-fw fa-upload"></i> Select Picture
								</button>
								<div class="image-container-preview">
									<img src="{{ Storage::disk('public')->url($currentPost['banner']) }}">
									<small class="d-block text-muted mt-2">** Leave it blank if you don't want to change the image</small>
								</div>
								<input type="file" name="banner" accept="image/png, image/gif, image/jpeg" class="d-none">
							</div>
						</div>
						<div class="form-group">
							<label>Content</label>
							<div id="editor">{!! $currentPost['content'] !!}</div>
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="form-control">
								<option value="">Select Status</option>
								<option value="1" {{ ($currentPost['status'] == 1) ? 'selected' : '' }}>Publish</option>
								<option value="0" {{ ($currentPost['status'] == 0) ? 'selected' : '' }}>Draft</option>
							</select>
						</div>
					</div>
					<div class="card-footer">
						<button type="submit" class="btn btn-primary">
							<i class="fa fa-fw fa-save"></i> Save 
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
	<script src="https://cdn.ckeditor.com/ckeditor5/33.0.0/classic/ckeditor.js"></script>
	<script>

		/**
		 * Store Process
		 */
		const store = (element, event) => {
			event.preventDefault(); 

			formData = new FormData(); 

			formData.append('id', $('input[name="id"]').val()); 
			formData.append('title', $('input[name="title"]').val()); 
			formData.append('category_id', $('select[name="category_id"]').val()); 
			formData.append('status', $('select[name="status"]').val()); 
			formData.append('content', CKEditor.getData());
			formData.append('_method', 'PUT'); 

			file = $('input[name="banner"]')[0]; 
			if (file.files.length > 0) {
				formData.append('banner', file.files[0]);
			}

			showLoadingSwal();

			axios({
				url: `${window.baseURL}/admin/post`, 
				method: 'POST',
				headers: {
					'Content-Type': 'form-data'
				}, 
				data: formData, 
			})
			.then(resultJson => {
				Swal.close(); 
				Swal.fire('Success', resultJson.message, 'success'); 
				document.location.href = `${window.baseURL}/admin/post`; 
			})
			.catch(errorResponse => {
				Swal.close(); 
				handleErrorRequest(errorResponse); 
			})
		}; 

		/**
		 * Image Uploader
		 */
		$('.upload-image .btn').on('click', function () {
			file = $('.upload-image input[type="file"]'); 
			file.click(); 
		}); 

		/**
		 * On Change File Input
		 */
		$('.upload-image input[type="file"]').on('change', function () {
			if (this.files && this.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('.image-container-preview img').attr('src', e.target.result).removeClass('d-none');
		        }

		        reader.readAsDataURL(this.files[0]);
		    }
		}); 

		var CKEditor = null; 

		/**
		 * CKEditor
		 */
	    ClassicEditor
	        .create( document.querySelector( '#editor' ) )
	        .then(editor => {
	        	CKEditor = editor; 
	        })
	        .catch( error => {
	            console.error( error );
	        } );

	</script>
@endpush