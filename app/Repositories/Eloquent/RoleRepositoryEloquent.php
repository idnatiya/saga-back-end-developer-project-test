<?php 

namespace App\Repositories\Eloquent; 

use App\Models\Role; 
use App\Repositories\RoleRepository; 
use App\Repositories\Eloquent\BaseRepositoryEloquent; 

class RoleRepositoryEloquent extends BaseRepositoryEloquent implements RoleRepository
{
	/**
	 * Construct 
	 * 
	 * @param Role $model 
	 */
	public function __construct(Role $model)
	{
		$this->model = $model; 
	}
}