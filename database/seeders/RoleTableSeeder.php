<?php

namespace Database\Seeders;

use App\Models\Role; 
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::query()->delete();  

        Role::create([
            'name' => 'Admin', 
            'guard_name' => 'web', 
        ]); 

        Role::create([
            'name' => 'Author',
            'guard_name' => 'web', 
        ]); 
    }
}
