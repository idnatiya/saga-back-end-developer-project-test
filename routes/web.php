<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('index'); 
Route::get('/category/{slug}', 'CategoryController@index')->name('category'); 
Route::get('/post/{slug}', 'PostController@index')->name('post'); 

Route::get('/intagram/auth/callback', 'InstagramController@get');