<?php 

namespace App\Models; 

use Illuminate\Database\Eloquent\Model; 

class Role extends Model
{
	/**
	 * Table Name 
	 * 
	 * @var string
	 */
	protected $table = 'roles'; 

	/**
	 * Primary Key 
	 * 
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Fillable Mask Assignment 
	 * 
	 * @var array 
	 */
	public $fillable = ['name', 'guard_name']; 
}