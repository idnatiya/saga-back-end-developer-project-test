<?php

namespace App\Http\Requests\Admin\Post;

use App\Repositories\PostRepository; 
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth; 

class DeleteRequest extends FormRequest
{
    protected $postRepository; 

    /**
     * Construct 
     * 
     * @param PostRepository $postRepository 
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository; 
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $deletedImages = []; 
        foreach ($this->id as $id) {
            $post = $this->postRepository->find($this->id); 
            if ($post == null) {
                abort(404); 
            }

            if (Auth::user()->hasRole('Author')) {
                if ($post[0]['user_id'] !== Auth::user()->id) {
                    abort(403); 
                }
            }

            $deletedImages[] = $post[0]['banner'];
        }

        $this->merge([
            'deletedImages' => $deletedImages, 
        ]); 

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id.*' => 'required|exists:posts,id', 
            'deletedImages' => '', 
        ];
    }
}
