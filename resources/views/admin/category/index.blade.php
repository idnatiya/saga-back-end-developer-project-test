@extends('admin::layouts.mainapp')

@section('title', 'List All Category')

@section('content')
	<div class="section">
		<div class="section-header d-flex justify-content-between">
			<h1>Categories</h1>
			<div class="create-button">
				<a href="#" class="btn btn-success" onclick="showCreateForm(event)">
					<i class="fa fa-fw fa-plus"></i> Create New Category
				</a>
			</div>	
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header">
							<form class="form-inline" action="" onsubmit="doSearch(event)">
								<div class="form-group">
									<input name="keyword" autocomplete="off" type="text" class="form-control" placeholder="Search Category ... ">
								</div>
								<button class="btn btn-info">
									<i class="fa fa-fw fa-search"></i> Search
								</button>
							</form>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-sm table-striped" id="table">
									<thead>
										<tr>
											<th>#</th>
											<th>Name</th>
											<th class="text-center">Actions</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
									<tfoot>
										<tr>
											<th><input type="checkbox" name="checkAll"></th>
											<th colspan="2">
												<button type="button" class="btn btn-danger btn-sm" id="btn-deleted">
													<i class="fa fa-fw fa-trash"></i> Delete Selected Row
												</button>
											</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
						<div class="card-footer">
							<div id="pagination-container"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('admin::category.form')

@endsection

@push('scripts')
	<script type="text/javascript">

		const modalForm = $('#modalForm');

		const doSearch = (event) => {
			event.preventDefault(); 
			table.loadDataTables(); 
		}; 

		/**
		 * Check All
		 */
		$('[name="checkAll"]').on('change', function () {
			elements = $('[name="id[]"]'); 
			$.each(elements, (_, element) => {
				$(element).prop('checked', this.checked); 
			}); 
		}); 

		/**
		 * Delete Checked
		 */
		$('#btn-deleted').on('click', function () {
			elements = $('[name="id[]"]:checked'); 
			if (elements.length == 0) {
				Swal.fire('Warning', 'No row selected', 'warning');
				return false;  
			}

			deletedRow = []; 
			$.each(elements, (_, element) => {
				deletedRow.push(element.value); 
			})

			showDeleteConfirmation()
				.then(result => {
					if (result.status) {
						doDelete(deletedRow); 
					} 
				}); 
		}); 

		/**
		 * Do Delete
		 */
		const doDelete = (deletedRow) => {
			window.showLoadingSwal(); 
			axios({
				url: `${window.baseURL}/admin/category`,
				method: 'DELETE', 
				data: {
					_token: `${window.csrfToken}`,
					id: deletedRow
				}
			})
			.then(resultJson => {
				Swal.close(); 
				Swal.fire('Success', resultJson.message, 'success'); 
				table.loadDataTables(); 
			})
			.catch(errorResponse => {
				Swal.close(); 
				handleErrorRequest(errorResponse); 
			}); 
		}; 

		/**
		 * Reset Input 
		 */
		const resetInput = () => {
			return new Promise(resolve => {
				modalForm.find('input[name="id"]').val(``); 
				modalForm.find('input[name="name"]').val(``); 

				resolve(); 
			}); 
		}; 

		/**
		 * Submit Action
		 */
		const doSubmit = (element, event) => {
			event.preventDefault(); 
			$element = $(element); 
			$element.find('button[type="submit"]')
				.addClass('disabled')
				.attr('disabled', 'disabled')
				.html(`<i class="fa fw-fw fa-circle-notch fa-spin"></i> Sending data`); 

			axios({
				url: $element.attr('action'),
				method: $element.attr('method'), 
				data: $element.serialize(), 
			})
			.then(resultJson => {
				Swal.fire('Success', resultJson.message, 'success'); 
				table.loadDataTables(); 
				$element.find('button[type="submit"]')
					.removeClass('disabled')
					.removeAttr('disabled')
					.html(`Save`); 
				resetInput().then(() => {
					modalForm.modal('hide'); 
				});
			})
			.catch(errorResponse => {
				handleErrorRequest(errorResponse);
				$element.find('button[type="submit"]')
					.removeClass('disabled')
					.removeAttr('disabled')
					.html(`Save`); 
			}); 
		}; 

		/**
		 * Show Create Form
		 */
		const showCreateForm = (event) => {
			event.preventDefault(); 
			modalForm.find('.modal-title').html('Create New Category'); 
			modalForm.find('[name="_method"]').val('POST'); 
			modalForm.find('.password-description').removeClass('d-block').addClass('d-none'); 
			resetInput().then(() => {
				modalForm.modal('show'); 
			}); 
		}; 

		/**
		 * Edit Row
		 */
		const editRow = (element, event) => {
			event.preventDefault(); 
			$element = $(element); 
			data = $element.data('json'); 
			
			new Promise(resolve => {
				modalForm.find('.modal-title').html('Update Category'); 
				modalForm.find('[name="_method"]').val('PUT'); 
				modalForm.find('input[name="id"]').val(data.id); 
				modalForm.find('input[name="name"]').val(data.name); 

				resolve(); 
			})
			.then(() => {
				modalForm.modal('show'); 
			})
		}; 

		/**
		 * Delete Row
		 */
		const deleteRow = (element, event) => {
			event.preventDefault(); 
			
			$el = $(element); 

			deletedRow = [$el.data('id')]; 

			showDeleteConfirmation()
				.then(result => {
					if (result.status) {
						doDelete(deletedRow); 
					} 
				}); 
		}	

		/**
		 * Table 
		 */
		const table = {
			baseURL: `{{ route('admin.category.tables') }}`, 
			element: $('#table'),
			setLoadingState() {
				this.element.find('tbody').html(`
					<tr class="text-warning">
						<td colspan="${this.element.find('thead th').length}">
							<i class="fa fa-fw fa-circle-notch fa-spin"></i> Please wait, loading data ...
						</td>
					</tr>
				`); 
			},
			render(resultJson) {
				let htmlRows = ``; 
				resultJson.forEach(row => {
					let htmlRow = $(`
						<tr>
							<td><input type="checkbox" value="${row.id}" name="id[]" /></td>
							<td class="td-name"></td>							
							<td class="text-center">
								<a href="#" onclick="editRow(this, event)" data-json='${JSON.stringify(row)}' class="btn btn-sm btn-warning">
									<i class="fa fa-fw fa-edit"></i> Edit 
								</a>
								<a href="#" data-id="${row.id}" onclick="deleteRow(this, event)" class="btn btn-sm btn-danger">
									<i class="fa fa-fw fa-trash"></i> Delete 
								</a>
							</td>
						</tr>
					`); 

					htmlRow.find('.td-name').text(row.name); 

					htmlRows += '<tr>' + htmlRow.html() + '</tr>';  
				}); 

				this.element.find('tbody').html(htmlRows); 
			}, 
			loadDataTables() {
				this.setLoadingState(); 
				axios({
					url: this.baseURL, 
					method: 'GET', 
					params: {
						keyword: $('input[name="keyword"]').val(), 
					}
				})
				.then(resultJson => {
					this.render(resultJson.data.data); 
					renderPagination(resultJson.data.links, $('#pagination-container'), this);
				})
				.catch(errorResponse => {
					handleErrorRequest(errorResponse); 
				}); 
			}
		};

		table.loadDataTables(); 

	</script>
@endpush