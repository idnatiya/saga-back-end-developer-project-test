<?php 

namespace App\Repositories\Eloquent; 

use App\Models\Post; 
use App\Repositories\PostRepository; 
use App\Repositories\Eloquent\BaseRepositoryEloquent; 

class PostRepositoryEloquent extends BaseRepositoryEloquent implements PostRepository
{
	/**
	 * Construct 
	 * 
	 * @param Post $model 
	 */
	public function __construct(Post $model)
	{
		$this->model = $model; 
	}

	/**
	 * Get Admin Table
	 */
	public function getAdminTable($requestData)
	{
		$postData = $this->model->query(); 
		$postData = $postData->with(['category', 'author']); 
		$postData = $postData->whereHas('category'); 
		$postData = $postData->whereHas('author'); 

		if (isset($requestData['keyword'])) {
			$postData = $postData->where(function ($query) use ($requestData) {
				$query->where('title', 'like', '%'.$requestData['keyword'].'%'); 
			}); 		
		}

		$postData = $postData->orderBy('id', 'DESC'); 
		$postData = $postData->paginate(10); 
		$postData = $postData->toArray(); 

		return $postData; 
	}

	/**
	 * Get Auhor Table
	 */
	public function GetPostByAuthor($userId, $requestData)
	{
		$postData = $this->model->query(); 
		$postData = $postData->with(['category', 'author']); 
		$postData = $postData->whereHas('category'); 
		$postData = $postData->whereHas('author'); 
		$postData = $postData->where('user_id', $userId); 

		if (isset($requestData['keyword'])) {
			$postData = $postData->where(function ($query) use ($requestData) {
				$query->where('title', 'like', '%'.$requestData['keyword'].'%'); 
			}); 		
		}

		$postData = $postData->orderBy('id', 'DESC'); 
		$postData = $postData->paginate(10); 
		$postData = $postData->toArray(); 

		return $postData; 
	}

	/**
	 * Get Where 
	 * 
	 * @param  mix $column 
	 * @param  mix $value  
	 * @return mix
	 */
	public function getWhere($column, $value)
	{
		$data = $this->model->query(); 
		$data = $data->with(['category', 'author']); 
		$data = $data->whereHas('category'); 
		$data = $data->whereHas('author'); 
		$data = $data->where('status', 1); 
		$data = $data->where($column, $value); 
		$data = $data->first(); 

		if ($data) {
			return $data->toArray(); 
		}

		return null;
	}

	/**
	 * Get Post By Category
	 */
	public function getPostByCategory($categoryId)
	{
		$postData = $this->model->where('category_id', $categoryId); 
		$postData = $postData->with(['category', 'author']); 
		$postData = $postData->whereHas('category'); 
		$postData = $postData->whereHas('author'); 
		$postData = $postData->where('status', 1); 
		$postData = $postData->orderBy('id', 'DESC'); 
		$postData = $postData->paginate(5);
		$postData = $postData->toArray(); 
		
		return $postData;
	}

	/**
	 * Get Last Posts 
	 */
	public function getLastPosts()
	{
		$postData = $this->model->query(); 
		$postData = $postData->with(['category', 'author']); 
		$postData = $postData->whereHas('category'); 
		$postData = $postData->whereHas('author'); 
		$postData = $postData->where('status', 1); 
		$postData = $postData->orderBy('id', 'DESC'); 
		$postData = $postData->limit(5); 
		$postData = $postData->get(); 
		$postData = $postData->toArray(); 

		return $postData; 
	}
}