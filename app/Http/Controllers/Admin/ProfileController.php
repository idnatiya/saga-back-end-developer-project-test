<?php 

namespace App\Http\Controllers\Admin; 

use App\Http\Requests\Admin\Profile\ChangePasswordRequest; 
use App\Services\ChangePasswordService; 
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        return view('admin::profile'); 
    }

    public function update(
        ChangePasswordRequest $requestData,
        ChangePasswordService $service
    )
    {
        $requestData = $requestData->validated(); 
        
        $requestData['user_id'] = Auth::user()->id; 
        $requestData['old_hashed_password'] = Auth::user()->password; 

        $changePasswordProcess = $service->change($requestData); 

        return response()->json([
            'status'    => $changePasswordProcess['status'], 
            'message'   => $changePasswordProcess['message'], 
        ], $changePasswordProcess['responseCode']); 
    }
}